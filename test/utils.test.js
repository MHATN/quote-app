import puppeteer from "puppeteer";
import { getRandomValueOfArray, checkSameQuote } from "../src/utils";

describe("Unit Tests", () => {
    test("Get random value of an array", () => {
        const array = [5, 4, 6, 20];
        const value = getRandomValueOfArray(array);
        expect(array.includes(value)).toBeTruthy();
    });

    test("Get False of an empty array", () => {
        const value = getRandomValueOfArray([]);
        expect(value).toBeFalsy();
    });

    test("Check same quote", () => {
        const element = { innerText: "Test text" };
        const quote = { quote: "Test text" };
        const result = checkSameQuote(element, quote);
        expect(result).toBeTruthy();
    });

    test("Check not the same quote", () => {
        const element = { innerText: "Text" };
        const quote = { quote: "Test text" };
        const result = checkSameQuote(element, quote);
        expect(result).toBeFalsy();
    });
});

describe("E2E Tests", () => {
    test("Generate new quote", async () => {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.goto("http://localhost:5500/quote-app/build/");
        const oldQuote = await page.$eval("blockquote", (el) => el.innerText);
        await page.click("button");
        const newQuote = await page.$eval("blockquote", (el) => el.innerText);
        expect(oldQuote === newQuote).toBeFalsy();
        await browser.close();
    });
});
