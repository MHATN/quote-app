import "../style/main.css";
import data from "../data/quotes.json";
import { setQuote } from "./utils";

document.addEventListener(
    "DOMContentLoaded",
    () => {
        const blockquote = document.querySelector("blockquote");
        const author = document.querySelector("#h2");
        const button = document.querySelector("button");

        setQuote(blockquote, author, data);

        button.addEventListener("click", () => {
            setQuote(blockquote, author, data);
        });
    },
    false
);
