export const getRandomValueOfArray = (array) => {
    if (array.length) {
        const index = Math.floor(Math.random() * array.length);
        return array[index];
    } else {
        return null;
    }
};

export const checkSameQuote = (blockquote, value) => {
    return blockquote.innerText === value.quote;
};

export const setQuote = (blockquote, author, array) => {
    let value = getRandomValueOfArray(array);
    while (checkSameQuote(blockquote, value)) {
        value = getRandomValueOfArray(array);
    }
    blockquote.innerText = value.quote;
    author.innerText = value.author;
};
